import { addUrl, createThing, getSolidDataset, getThing, getUrlAll, removeAll, removeThing, saveSolidDatasetAt, setInteger, setStringNoLocale, setThing, Thing } from "@inrupt/solid-client";
import React from "react";
import { SessionProvider } from "@inrupt/solid-ui-react";
import { gameDocUrl, gameUrl, vocab } from "../vocab";
import { Admin } from "../components/Admin";

const liedjes = [
  {song: 'The White Stripes - My Doorbell',
  question: 'Hoe heet dit nummer?',
  answer: 'My Doorbell'}, {
  song: 'George Baker Selection - Little Green Bag',
  question: 'dit nummer is van de George Baker Selection. Waar komt deze band vandaan?',
  answer: 'Nederland'}, {
  song: 'Mark Ronson ft. Amy Winehouse - Valerie',
  question: 'gezongen door Amy Winehouse, maar samen met wie?',
  answer: 'Mark Ronson'}, {
  song: 'Robin Thicke - When I Get You Alone',
  question: 'In 2013 had Robin Thicke nog een veel grotere hit. Welk nummer was dat?',
  answer: 'Blurred Lines'}, {
  song: 'Rubberbandits - Black Man',
  question: 'waar komen deze Rubberbandits vandaan?',
  answer: 'Ierland'}, {
  song: 'Fools Garden - Lemon Tree',
  question: 'wat is de significantie van citroenen in stillevens?',
  answer: 'Welvaart'}, {
  song: 'Bob Marley & The Wailers - No Woman No Cry',
  question: 'Bob Marley was een Rastafari. Hoe heet de god van Rastafari’s?',
  answer: 'Jah'}, {
  song: 'Elvis Presley vs. Junkie XL - A Little Less Conversation',
  question: 'een remix van Elvis door JXL - welke artiest wordt tot zijn ongenoegen wel eens vergeleken met Elvis, omdat hij ook een witte man is die zwarte muziek maakt.',
  answer: 'Eminem'}, {
  song: 'Het Goede Doel - België (Is er leven op Pluto)',
  question: 'Waarom wilde Henk niet naar Amerika?',
  answer: 'Niet echt'}, {
  song: 'The Proclaimers - I’m Gonna Be (500 Miles)',
  question: 'vanuit welk land lopen The Proclaimers 500 mijl?',
  answer: 'Schotland'}, {
  song: 'Don McLean - American Pie',
  question: 'hoe heet het op één na bekendste nummer van Don McLean?',
  answer: 'Vincent'}, {
  song: 'Justice vs. Simian - We Are Your Friends',
  question: 'hoe heet de “justice” van het Amerikaanse supreme court die recent is overleden, en voor wie Trump tegen haar wens in Amy Coney Barrett als vervanger heeft genomineerd?',
  answer: 'Ruth Bader Ginsburg'}, {
  song: 'Portugal. The Man - Feel It Still',
  question: 'uit welk verhaal komt het gezegde “down the rabbit hole”?',
  answer: 'Alice In Wonderland'}, {
  song: 'Tame Impala - Lost in Yesterday',
  question: 'hoeveel verschillende premiers heeft Australië tussen 2010 en 2020 gehad?',
  answer: 'Zes'}, {
  song: 'Pink Floyd - The Great Gig in the Sky',
  question: 'wat staat er op de hoes van dit legendarische album?',
  answer: 'Een prisma'}, {
  song: 'Fatboy Slim - The Rockafeller Skank',
  question: 'welke versie van FIFA gebruikte dit nummer?',
  answer: '99'}, {
  song: 'Gnarls Barkley - Crazy',
  question: 'Met wie vormt CeeLo Green, ook bekend van bijv. het nummer “Fuck you”, het duo Gnarls Barkley?',
  answer: 'Danger Mouse'}, {
  song: 'OutKast - Hey Ya!',
  question: 'waarom moest je een Polaroidfoto schudden? (Hoeft niet bij de foto’s met de iconische rand.)',
  answer: 'Drogen'}, {
  song: 'Le Le - Luxe Benen',
  question: 'hoeveel inwoners heeft de Benelux?',
  answer: '30 miljoen'}, {
  song: 'The Police - Roxanne',
  question: 'een veelgehoorde slogan van BLM-protesteerders in Amerika is “defund the police!” Willen ze daadwerkelijk de politie opheffen?',
  answer: 'Nee, ze willen alleen dat er minder geld gestoken wordt in militair materieel, en dat er in plaats daarvan fors wordt geïnvesteerd in preventie, zoals in anti-armoedeprogramma’s.'}, {
  song: 'The Raconteurs - Hands',
  question: 'hoeveel vingers zitten er aan één gemiddelde hand?',
  answer: 'Vijf'}, {
  song: 'The Partysquad - Non Stop',
  question: 'Wat was Brainpowers eerste nummer 1-hit?',
  answer: 'Dansplaat'}, {
  song: 'Guns ‘n Roses - Paradise City',
  question: 'Hoe heet de zanger van Guns ‘n Roses?',
  answer: 'Axl Rose'}, {
  song: 'David Bowie & Mick Jagger - Dancing in the Street',
  question: 'Hoe heet de band van Mick Jagger?',
  answer: 'Rolling Stones'}, {
  song: 'Metronomy - The Look',
  question: 'Dit nummer komt van het album “The English Riviera”. Wat betekent “Riviera”?',
  answer: 'Kust'}, {
  song: 'Lorde - Royals',
  question: 'in het programma South Park, van welk personage is Lorde het geheime alter ego?',
  answer: 'Randy Marsh'}, {
  song: 'Maribou State ft. Holly Walker - Midas',
  question: 'waarin verander je als je geraakt wordt door de “midas touch”?',
  answer: 'Goud'}, {
  song: 'LP - When We’re High',
  question: 'waarvan is een langspeelplaat gemaakt?',
  answer: 'Vinyl'}, {
  song: 'Steve Monite - Only You',
  question: 'hoeveel verschillende talen worden er in Nigeria gesproken?',
  answer: '>500'}, {
  song: 'Alabama Shakes - Don’t Wanna Fight',
  question: 'Alabama is waar Forrest Gump vandaag komt. Welke acteur speelt Forrest Gump?',
  answer: 'Tom Hanks'}, {
  song: 'Eminem - Without Me',
  question: 'als welke superheldensidekick verkleed Eminem zich wel eens?',
  answer: 'Robin'},
  { song: 'Bonobo - Ketto',
  question: 'In welk land komen Bonobo\'s voor?',
  answer: 'Congo-Kinshasa'},
  { song: 'Big Brovaz - Nu Flo',
  question: 'Uit welk boek komt het begrip "Big Brother"?',
  answer: '1984'},
  { song: 'The Caesars - Jerk It Out',
  question: 'Wat voor sla gaat er in een Caesarsalade?',
  answer: 'Romaine'},
  { song: 'Tom Misch - South of the River',
  question: 'South of welke river in Londen?',
  answer: 'Thames'},
  { song: 'Doe Maar - Smoorverliefd',
  question: '"Skunk" zou een combinatie van ska en punk zijn, maar waar is het nog meer een naam voor?',
  answer: '(Neder)wiet'},
];
shuffleArray(liedjes);

const Baas: React.FC = () => {
  React.useEffect(() => {
    (async () => {
      const ds = await getSolidDataset(gameDocUrl);
      const game = getThing(ds, gameUrl) as Thing;
      const questions = getUrlAll(game, vocab.questions);
      if(questions.length !== liedjes.length) {
        let updatedDs = questions.reduce((ds, questionUrl) => removeThing(ds, questionUrl), ds)
        let updatedGame = removeAll(game, vocab.questions);
        const newThings = liedjes.map((liedje, i) => {
          let liedjeThing = createThing({ name: i.toString() });
          liedjeThing = setStringNoLocale(liedjeThing, vocab.song, liedje.song);
          liedjeThing = setStringNoLocale(liedjeThing, vocab.question, liedje.question);
          liedjeThing = setStringNoLocale(liedjeThing, vocab.answer, liedje.answer);
          return liedjeThing;
        });
        updatedGame = newThings.reduce((gameSoFar, liedjeThing) => addUrl(gameSoFar, vocab.questions, liedjeThing), updatedGame);
        updatedGame = setInteger(updatedGame, vocab.currentQuestion, 0);
        updatedDs = newThings.reduce(setThing, setThing(updatedDs, updatedGame));
        await saveSolidDatasetAt("https://vincentt.inrupt.net/public/muziekbingo/game.ttl", updatedDs);
        console.log('Saved!');
      }
    })();
  }, []);

  return (
    <SessionProvider sessionId="bingokaartSessionId">
      <Admin/>
    </SessionProvider>
  );
};

export default Baas;

function shuffleArray<T>(array: T[]) {
  for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
  }
}
