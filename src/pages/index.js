import Head from 'next/head'
import Link from 'next/link';
import styles from '../../styles/Home.module.css'

export default function Home() {
  return (
    <div style={{ padding: '1rem 2rem' }}>
      <p>Voor je begint: ik heb dit ding vandaag gemaakt, dus ja, als je een beetje kunt programmeren kun je de antwoorden bekijken en shit kapot maken - dus doe maar niet :P</p>
      <p>
        Nou, op hoop van zegen dat het niet kapot gaat:
      </p>
      <Link href="./speler.tsx" as="/speler">
        <a className={styles.linkje}>Let's goooooo!</a>
      </Link>
    </div>
  )
}
