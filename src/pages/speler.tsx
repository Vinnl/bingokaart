import LocalForage from 'localforage';
import React from 'react';
import { v4 } from 'uuid';
import { Kaart } from '../components/Kaart';

const Speler: React.FC = () => {
  const uuid = useUuid();

  if (!uuid) {
    return (
      <>
        Komt eraan, nog even geduld...
      </>
    );
  }

  return <Kaart uuid={uuid}/>;
};

function useUuid(): string | undefined {
  const [uuid, storeUuid] = useLocalForage('uuid');

  if (uuid === null) {
    storeUuid(v4());
    return undefined;
  }

  return uuid;
}

function useLocalForage(key: string): [undefined | null | string, (value: string) => void] {
  const [value, setValue] = React.useState<string | null>();

  React.useEffect(() => {
    LocalForage.getItem(key).then((value: unknown) => setValue((value as string | null)));
  }, [key])

  function updateValue(value: string) {
    LocalForage.setItem(key, value).then(() => setValue(value));
  }

  return [value, updateValue];
}

export default Speler;
