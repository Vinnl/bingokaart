import { useState } from "react";
import styles from "../../styles/Naamkiezer.module.scss";

interface Props {
  onSet: (name: string) => void,
}

export const Naamkiezer: React.FC<Props> = (props) => {
  const [naam, setNaam] = useState<string>();

  const onSubmit: React.FormEventHandler = (e) => {
    e.preventDefault();

    if (naam) {
      props.onSet(naam);
    }
  };

  return (
    <>
      <form onSubmit={onSubmit} className={styles.naamkiezer}>
        <label htmlFor="name">Wat is je naam?</label>
        <input type="text" required id="name" onChange={(e) => setNaam(e.target.value)}/>
        <input type="submit" value="Geef me een kaart!"/>
        <p>(Wel een unieke naam AUB.)</p>
      </form>
    </>
  );
};
