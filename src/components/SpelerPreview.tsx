import { getInteger, getSourceUrl, getStringNoLocale, getThing, getUrl, getUrlAll, SolidDataset, Thing, WithResourceInfo } from "@inrupt/solid-client";
import { gameUrl, vocab } from "../vocab";
import styles from "../../styles/SpelerPreview.module.scss";

interface Props {
  spelerDataset: SolidDataset & WithResourceInfo;
  gameDataset: SolidDataset;
}

export const SpelerPreview: React.FC<Props> = (props) => {
  const card = getThing(props.spelerDataset, getSourceUrl(props.spelerDataset) + "#card")!;

  const name = getStringNoLocale(card, vocab.playerName)!;

  const squares: JSX.Element[] = [];

  const game = getThing(props.gameDataset, gameUrl)!;
  const current = getInteger(game, vocab.currentQuestion) ?? 0;
  const doneAnswers = getUrlAll(game, vocab.questions).filter(questionUrl => Number.parseInt(questionUrl.substring(questionUrl.indexOf("#") + 1)) <= current);

  for(let row = 0; row < 5; row++) {
    for(let column = 0; column < 5; column++) {
      if (row === 2 && column === 2) {
        squares.push(<span>✓</span>);
        continue;
      }

      const squareThing = getThing(props.spelerDataset, getUrl(card, vocab.squarePrefix + `-${row}-${column}`)!)!;
      const checked = getInteger(squareThing, vocab.check) === 1;
      const answerUrl = getUrl(squareThing, vocab.squareValue)!;
      const answerThing = getThing(props.gameDataset, answerUrl)!;

      let className = checked ? styles.checked : "";
      className += " ";
      if(doneAnswers.includes(answerUrl)) {
        className += styles.done;
      }

      squares.push(
        <span className={className}>
          {getStringNoLocale(answerThing, vocab.answer)}
        </span>
      );
    }
  }

  return (
    <div>
      {name}
      <div className={styles.kaart}>
        {squares}
      </div>
    </div>
  );
};
