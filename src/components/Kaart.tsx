import styles from "../../styles/Kaart.module.scss";
import { getInteger, getStringNoLocale, getThing, getUrl, setInteger, setThing, SolidDataset, Thing, UrlString } from "@inrupt/solid-client";
import { initialiseSpelerDataset } from "../functions/intialiseSpelerDataset";
import { useDataset } from "../hooks/useDataset";
import { gameDocUrl, playersContainerUrl, podUrl, vocab } from "../vocab";
import { Naamkiezer } from "./Naamkiezer";

interface Props {
  uuid: string;
}

export const Kaart: React.FC<Props> = (props) => {
  const playerDocUrl = `${playersContainerUrl}${props.uuid}.ttl`;
  const [spelerDataset, setSpelerDataset] = useDataset(playerDocUrl);
  const [mainDataset] = useDataset(gameDocUrl);

  if (typeof spelerDataset === "undefined" || !mainDataset) {
    return <>Rustââgh!</>;
  }

  const card = spelerDataset
    ? getThing(spelerDataset, playerDocUrl + "#card")
    : undefined;
  const naam = card
    ? getStringNoLocale(card, vocab.playerName)
    : undefined;

  function setNaam(naam: string) {
    const newSpelerDataset = initialiseSpelerDataset(mainDataset!, naam);
    setSpelerDataset(newSpelerDataset);
  }

  if (typeof naam === "undefined") {
    // Name will only be undefined if spelerDataset is not initialised yet
    return (
      <Naamkiezer onSet={(val: string) => setNaam(val)}/>
    );
  }

  if(!spelerDataset) {
    throw new Error("Hmm, daar ging iets mis. Je mag boos worden op Vincent. Maar eigenlijk niet, want hij is bijna jarig, dus deal with it.")
  }

  const setChecked = (square: Thing, checked: boolean) => {
    const updatedThing = setInteger(square, vocab.check, checked ? 1 : 0);
    const updatedDataset = setThing(spelerDataset, updatedThing);
    setSpelerDataset(updatedDataset);
  }

  const rows: JSX.Element[][] = [];
  for(let row = 0; row < 5; row++) {
    rows[row] = [];
    for(let column = 0; column < 5; column++) {
      if (row === 2 && column === 2) {
        rows[row][column] = <span>✓</span>;
        continue;
      }
      const square = getThing(spelerDataset, `${playerDocUrl}#square-${row}-${column}`)!;
      const answer = getAnswer(mainDataset, getUrl(square, vocab.squareValue)!);
      const isChecked = getInteger(square, vocab.check) === 1 ? true : false;
      rows[row][column] = (
        <button
          type="button"
          className={isChecked ? styles.checked : styles.unchecked}
          onClick={(e) => {
            e.preventDefault();
            setChecked(square, !isChecked);
          }}
        >
          {answer}
        </button>
      );
    }
  }

  return (
    <div className={styles.kaart}>
      {rows}
    </div>
  );
};

function getAnswer(mainDataset: SolidDataset, liedjeUrl: UrlString): string {
  return getStringNoLocale(getThing(mainDataset, liedjeUrl)!, vocab.answer)!;
}
