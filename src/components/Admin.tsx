import { getInteger, getStringNoLocale, getThing, setInteger, setThing } from "@inrupt/solid-client";
import { LoginButton, useSession } from "@inrupt/solid-ui-react";
import { useDataset } from "../hooks/useDataset";
import { usePlayerDatasets } from "../hooks/usePlayersDatasets";
import { gameDocUrl, gameUrl, vocab } from "../vocab";
import { SpelerPreview } from "./SpelerPreview";

interface Props {
}

export const Admin: React.FC<Props> = (props) => {
  const { session } = useSession();
  const [gameDoc, setGameDoc] = useDataset(gameDocUrl);
  const playerDatasets = usePlayerDatasets(gameDoc);

  if (!session.info.isLoggedIn) {
    return <LoginButton oidcIssuer="https://inrupt.net" redirectUrl={typeof window !== "undefined" ? window.location.href : ""}/>; 
  }

  if(!gameDoc) {
    return <>Jo wacht ff gozert</>;
  }

  const game = getThing(gameDoc, gameUrl)!;
  const current = getInteger(game, vocab.currentQuestion) ?? 0;

  const currentQuestion = getThing(gameDoc, gameDocUrl + "#" + current);

  const setCurrent = (newCurrent: number) => {
    const updatedGame = setInteger(game, vocab.currentQuestion, newCurrent);
    const updatedDataset = setThing(gameDoc, updatedGame);
    setGameDoc(updatedDataset);
  }

  const sortButtons = (
    <>
      <button
        type="button"
        onClick={(e) => {
          e.preventDefault();
          setCurrent(current - 1);
        }}
      >
        Vorige
      </button>
      <button
        type="button"
        onClick={(e) => {
          e.preventDefault();
          setCurrent(current + 1);
        }}
      >
        Volgende
      </button>
    </>
  );

  const players = (
    <>
      <hr/>
      {playerDatasets.map((playerDataset, i) => <SpelerPreview key={i} spelerDataset={playerDataset} gameDataset={gameDoc}/>)}
    </>
  );

  if(!currentQuestion) {
    return <>Dat was 'm! {sortButtons}{players}</>;
  }

  return (
    <div>
      <dl>
        <dt>Nummer:</dt>
        <dd>{getStringNoLocale(currentQuestion, vocab.song)}</dd>
        <dt>Vraag:</dt>
        <dd>{getStringNoLocale(currentQuestion, vocab.question)}</dd>
        <dt>Antwoord:</dt>
        <dd>{getStringNoLocale(currentQuestion, vocab.answer)}</dd>
      </dl>
      {sortButtons}
      {players}
    </div>
  );
};
