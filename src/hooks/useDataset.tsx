import { getSolidDataset, saveSolidDatasetAt, SolidDataset, UrlString } from "@inrupt/solid-client";
import { useSession } from "@inrupt/solid-ui-react";
import React from "react";

export function useDataset(url: UrlString) {
  const [dataset, setDataset] = React.useState<SolidDataset | null>();
  const session = useSession();
  const fetch = session ? session.fetch : window.fetch;

  React.useEffect(() => {
    getSolidDataset(url, { fetch: fetch }).then((fetchedDataset) => {
      setDataset(fetchedDataset);
    }).catch((e) => {
      setDataset(null);
    });
  }, [url]);

  async function updateDataset(changedDataset: SolidDataset) {
    try {
      const updatedDataset = await saveSolidDatasetAt(url, changedDataset, { fetch: fetch });
      setDataset(updatedDataset);
    } catch (e) {
      console.log("Opslaan is helaas mislukt:", e);
    }
  }

  return [dataset, updateDataset] as const;
}
