import { getDatetime, getSolidDataset, getSourceUrl, getThing, getUrlAll, saveSolidDatasetAt, SolidDataset, UrlString, WithResourceInfo } from "@inrupt/solid-client";
import { useSession } from "@inrupt/solid-ui-react";
import React, { useEffect } from "react";
import { dct, ldp } from "rdf-namespaces";
import { playersContainerUrl } from "../vocab";
import { useDataset } from "./useDataset";

export function usePlayerDatasets(gameDataset?: SolidDataset | null) {
  const session = useSession();
  const fetch = session ? session.fetch : window.fetch;
  const [playerDatasets, setPlayerDatasets] = React.useState<(SolidDataset & WithResourceInfo)[]>([]);

  useEffect(() => {
    if(!gameDataset) {
      return;
    }

    (async () => {
      const containerDataset = await getSolidDataset(playersContainerUrl, { fetch: fetch });
      const containerThing = getThing(containerDataset, playersContainerUrl)!;
      const playerUrls = getUrlAll(containerThing, ldp.contains).filter(url => !url.endsWith(".lock") && !url.endsWith(".lock/"));
      const datasets = await Promise.all(playerUrls.map(ds => getSolidDataset(ds, { fetch: fetch })));
      setPlayerDatasets(datasets.sort(sortByDate));
    })();
  }, [gameDataset]);

  return playerDatasets;
}

function sortByDate(dsA: SolidDataset & WithResourceInfo, dsB: SolidDataset & WithResourceInfo): number {
  const cardA = getThing(dsA, getSourceUrl(dsA) + "#card")!;
  const cardB = getThing(dsB, getSourceUrl(dsB) + "#card")!;
  const dateA = getDatetime(cardA, dct.created);
  const dateB = getDatetime(cardB, dct.created);
  if (dateA === null || dateB === null) {
    return 0;
  }
  return dateA.getTime() - dateB.getTime();
}
