import { addUrl, createSolidDataset, createThing, getThing, getUrlAll, setDatetime, setStringNoLocale, setThing, setUrl, SolidDataset, Thing, UrlString } from "@inrupt/solid-client";
import { dct, rdf, rdfs } from "rdf-namespaces";
import { gameUrl, vocab } from "../vocab";

export function initialiseSpelerDataset(mainDataset: SolidDataset, naam: string): SolidDataset {
  let spelerDataset = createSolidDataset();

  let card = createThing({ name: "card" });
  const squares: Thing[] = [];

  const answers = pick24Answers(mainDataset);
  for(let row = 0; row < 5; row++) {
    for(let column = 0; column < 5; column++) {
      if (row === 2 && column === 2) {
        // Center square is free
        continue;
      }
      let squareThing = createThing({ name: `square-${row}-${column}` });
      squareThing = setUrl(squareThing, vocab.squareValue, answers[row * 5 + column]);
      card = addUrl(card, `${vocab.squarePrefix}-${row}-${column}`, squareThing);
      squares.push(squareThing);
    }
  }

  card = setStringNoLocale(card, vocab.playerName, naam);
  card = setDatetime(card, dct.created, new Date());

  spelerDataset = setThing(spelerDataset, card);
  spelerDataset = squares.reduce(setThing, spelerDataset);

  return spelerDataset;
}

function pick24Answers(mainDataset: SolidDataset): UrlString[] {
  const answers: UrlString[] = [];
  const game = getThing(mainDataset, gameUrl)!;
  const potentialAnswers = getUrlAll(game, vocab.questions);

  while(answers.length <= 24) {
    const proposedAnswer = getRandomElement(potentialAnswers);
    if (!answers.includes(proposedAnswer)) {
      answers.push(proposedAnswer);
    }
  }

  return answers;
}

function getRandomElement(array: UrlString[]): UrlString {
  return array[Math.floor(Math.random() * array.length)];
}
