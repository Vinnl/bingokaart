export const vocab = {
  questions: "https://vocabularies.vincenttunru.com/bingo#questions",
  song: "https://vocabularies.vincenttunru.com/bingo#song",
  question: "https://vocabularies.vincenttunru.com/bingo#question",
  answer: "https://vocabularies.vincenttunru.com/bingo#answer",
  squarePrefix: "https://vocabularies.vincenttunru.com/bingo#square",
  currentQuestion: "https://vocabularies.vincenttunru.com/bingo#currentQuestion",
  squareValue: "https://vocabularies.vincenttunru.com/bingo#squareValue",
  check: "https://vocabularies.vincenttunru.com/bingo#check",
  playerName: "https://vocabularies.vincenttunru.com/bingo#playerName",
};

export const podUrl = "https://vincentt.inrupt.net";
export const gameDocUrl = podUrl + "/public/muziekbingo/game.ttl";
export const gameUrl = gameDocUrl + "#game";
export const playersContainerUrl = podUrl + "/public/muziekbingo/players/";
