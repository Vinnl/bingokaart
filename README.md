This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

It was created just to serve my birthday, so don't use this as an example of best practices.

That said, if you happen to be called Michiel and would like to use this yourself, this is what I think you'd need to do:

- Search for `vincentt.inrupt.net` and make sure to replace it with the URL to your own Pod.
- Create `/public/muziekbingo/game.ttl` and `/public/muziekbingo/players`. Make the former publicly writable, and the latter publicly appendable, and children of the latter publicly writable.
- Insert your songs titles, questions and answers into `baas.tsx`. If you now run the app (`yarn run dev`) and visit `/baas`, and `game.ttl` in your Pod does not yet contain the same number of questions as are listed there, it will store them in your Pod in a random order.
- Once you're happy with the questions in your Pod, you can set `game.ttl` back to publicly-readable again.
- To deploy the app, run `yarn run build`, then `yarn run export`, then upload the contents of the `out` directory to wherever you want to host it.
- Direct participants to `/speler`, where they will be able to obtain a bingo card. You can visit `/baas`, where you can log in to your Pod, then see everyone's cards. Every time you move to another question, it will update the list of everyone's cards, and it will highlight which squares you've already passed (marked in bold) and which squares the players have checked (marked with a non-white background colour).
- Have fun!
